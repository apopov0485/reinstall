:: Install all the packages
:::: Browsers
choco install googlechrome -fy
choco install firefox -fy


:::: Dev tools
choco install git -fy


:::: Media
choco install vlc -fy

:::: Utilities + other
choco install 7zip.install -fy
choco install dropbox -fy
choco install slack -fy
choco install adobereader-fy
choco install steam-client -fy
choco install avirafreeantivirus -fy