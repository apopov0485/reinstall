#!/bin/bash

sudo apt update

sudo apt install -y wget curl apt-transport-https ca-certificates curl software-properties-common sshfs lm-sensors

sudo apt update

# Chrome install

echo "Install google chrome"
echo

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb


sudo apt install ./google-chrome-stable_current_amd64.deb

cat /etc/apt/sources.list.d/google-chrome.list 
rm ./google-chrome-stable_current_amd64.deb

cp ./google-chrome.desktop /home/$USER/Bureau/google-chrome.desktop


echo "Install chromium"
echo

sudo apt install -y chromium-browser

echo "Install git"
echo

sudo apt install -y git

echo "Install Docker"
echo

sudo apt -y remove docker docker-engine docker.io containerd runc

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg


echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu jammy stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

cat /etc/apt/sources.list.d/docker.list

sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-compose



sudo usermod -aG docker $USER
newgrp docker


echo "ver num activate"
# Menu > Administration > Écran de connexion / cocher dans option

sudo apt install numlockx


echo "utility soft"
echo

sudo apt-get install doublecmd-gtk pavucontrol rar p7zip-rar catfish vlc gparted


echo "VPN boite"
echo

sudo add-apt-repository ppa:nm-l2tp/network-manager-l2tp

sudo apt-get install network-manager-l2tp network-manager-l2tp-gnome

echo "Jet brain tool box"


curl -fsSL https://raw.githubusercontent.com/nagygergo/jetbrains-toolbox-install/master/jetbrains-toolbox.sh | bash
jetbrains-toolbox

echo "Teams"

curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | sudo tee /usr/share/keyrings/ms-teams.gpg > /dev/null

sudo apt update -y

sudo apt install teams

sudo rm /etc/apt/sources.list.d/ms-teams.list

echo "Vs code"

wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'



sudo apt-get install apt-transport-https
sudo apt-get update

sudo apt-get install code
