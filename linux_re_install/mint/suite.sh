#!/bin/bash

sudo apt -y install numlockx


echo "utility soft"
echo

sudo apt -y install doublecmd-gtk pavucontrol rar p7zip-rar catfish vlc gparted


echo "VPN boite"
echo

sudo add-apt-repository ppa:nm-l2tp/network-manager-l2tp

sudo apt -y install network-manager-l2tp network-manager-l2tp-gnome

echo "Jet brain tool box"


curl -fsSL https://raw.githubusercontent.com/nagygergo/jetbrains-toolbox-install/master/jetbrains-toolbox.sh | bash
jetbrains-toolbox

echo "Teams"

curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | sudo tee /usr/share/keyrings/ms-teams.gpg > /dev/null

sudo apt update -y

sudo apt -y install teams

sudo rm /etc/apt/sources.list.d/ms-teams.list

echo "Vs code"

wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'



sudo apt-get -y install apt-transport-https
sudo apt-get update

sudo apt-get -y install code
